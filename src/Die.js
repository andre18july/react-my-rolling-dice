import React, { Component } from 'react';
import './Die.css';

class Die extends Component {

    render(){

        return(
            <div className="Die">
                <i class={`fas fa-dice-${this.props.val} fa-10x`}></i>
            </div>
        )
    }
}

export default Die;