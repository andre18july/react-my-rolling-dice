import React, { Component } from 'react';
import Die from './Die';
import './RollDice.css';

class RollDice extends Component {

    constructor(props){
        super(props);
        this.state = {
            dice1 : 'one',
            dice2 : 'two',
            isRolling: false
        }

    }

    getRandom = () => {
        const rand = Math.floor(Math.random() * 6 + 1);
        switch(rand){
            case 1: return 'one';
            case 2: return 'two';
            case 3: return 'three';
            case 4: return 'four';
            case 5: return 'five';
            case 6: return 'six';
            default: break;
        }
    }
    
    rollingDice = () => {
        const valDice1 = this.getRandom();
        const valDice2 = this.getRandom();

        this.setState({
            isRolling: true
        });

        setTimeout(() => {
            this.setState({
                dice1: valDice1,
                dice2: valDice2,
                isRolling: false
            });
        }, 1000);


    }


    render(){

        let roll = "";
        this.state.isRolling ? roll = "RollDice isRolling" : roll = "RollDice"; 

        return(
            <div>
                <h1 className="RollDice-Title">My Rolling Dice APP</h1>
                <div className= { roll }>
                    <Die val={this.state.dice1}></Die>
                    <Die val={this.state.dice2}></Die>
                </div>
                <button className="RollDice-Button" onClick={this.rollingDice}>{this.state.isRolling ? "Is Rolling..." : "Rolling Dice"}</button>
            </div>
        );
    }
}

export default RollDice;